/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_args.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/21 19:00:11 by vbudnik           #+#    #+#             */
/*   Updated: 2018/05/21 20:52:10 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/minishell.h"

/*
** free 2d array
*/

void		ft_freearr(char **arr)
{
	int		i;

	i = 0;
	if (arr == NULL)
		return ;
	while (arr[i] != NULL)
	{
		free(arr[i]);
		i++;
	}
	if (arr != NULL)
		free(arr);
}

/*
** get size 2d array
*/

int			ft_count_arr(char **arr)
{
	int		i;

	i = 0;
	while (arr[i])
	{
		i++;
	}
	return (i);
}

/*
** join argumets of 2d array, start of second element
*/

char		*ft_argsjoin(char **args)
{
	int		i;
	char	*str;
	char	*tmp;

	i = 1;
	str = "\0";
	tmp = "\0";
	while (args[i])
	{
		str = ft_strjoin(tmp, args[i]);
		if (i > 1)
			free(tmp);
		if (i < (ft_count_arr(args) - 1))
		{
			tmp = str;
			str = ft_strjoin(tmp, " ");
			free(tmp);
		}
		tmp = str;
		i++;
	}
	free(tmp);
	return (str);
}
