/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tehfunc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/22 13:16:26 by vbudnik           #+#    #+#             */
/*   Updated: 2018/05/22 13:20:37 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/minishell.h"
#include <pwd.h>

/*
** get user name
*/

char		*get_name(void)
{
	uid_t			uid;
	struct passwd	*pw;

	uid = getuid();
	pw = getpwuid(uid);
	if (pw)
		return (pw->pw_name);
	return (NULL);
}

/*
** check my function
*/

int			ft_myfunc(char *str)
{
	if (ft_strcmp(str, "echo") == 0 || ft_strcmp(str, "cd") == 0 ||
		ft_strcmp(str, "env") == 0 || ft_strcmp(str, "setenv") == 0 ||
		ft_strcmp(str, "unsetenv") == 0 || ft_strcmp(str, "clear") == 0
		|| ft_strcmp(str, "exit") == 0)
		return (1);
	return (0);
}

/*
** for norm in ft_loop
*/

void		ft_sokrati(char *linetmp, char *tmp)
{
	if (linetmp[0] == '\0')
		linetmp = NULL;
	if (tmp != NULL)
		free(tmp);
}

/*
** for norm in ft_loop
*/

void		ft_fin_free(char *str)
{
	if (str[0] == '\0')
		str = NULL;
	if (str != NULL)
		free(str);
}
