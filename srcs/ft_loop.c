/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_loop.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/21 18:40:58 by vbudnik           #+#    #+#             */
/*   Updated: 2018/06/30 15:20:55 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/minishell.h"

/*
** free array of struct env
*/

void			ft_freearg(t_env *e)
{
	int		i;

	i = 0;
	if (e->args != NULL)
		return ;
	while (e->args[i] != NULL)
	{
		free(e->args[i]);
		i++;
	}
	if (e->args != NULL)
		free(e->args);
}

/*
** free and exit from minishell
*/

void			ft_exit(t_env *e)
{
	ft_putendl("Goodbay");
	ft_freearg(e);
	ft_freearr(e->path);
	exit(0);
}

/*
**  check my function
*/

static int		ft_builtins(t_env *e)
{
	int		res;
	char	*str;

	str = e->args[0];
	res = 0;
	if (ft_myfunc(str))
		res = 1;
	if (res == 1)
	{
		if (ft_strcmp(str, "echo") == 0)
			ft_echo(e);
		else if (ft_strcmp(str, "cd") == 0)
			ft_cd(e);
		else if (ft_strcmp(str, "env") == 0)
			ft_env(e);
		else if (ft_strcmp(str, "setenv") == 0 && e->args[1] != NULL
				&& e->args[2] != NULL)
			ft_setenv(e->args[1], e->args[2], 1, e);
		else if (ft_strcmp(str, "unsetenv") == 0 && e->args[1] != NULL)
			ft_unsetenv(e->args[1], e);
		else if (ft_strcmp(str, "exit") == 0 && e->args[1] == NULL)
			ft_exit(e);
	}
	return (res);
}

/*
** printing name of minishell
*/

static void		ft_printp(t_env *e)
{
	if (getcwd(e->cwd, sizeof(e->cwd)) != 0)
	{
		ft_putstr(RED);
		ft_putstr(e->cwd);
		ft_putstr(RESET);
		ft_putstr("  ");
		ft_putstr(GRN);
		ft_putstr(get_name());
		ft_putstr(RESET);
		ft_putstr(YEL);
		ft_putstr("  minishellV_0.1\n");
		ft_putstr(RESET);
		ft_putstr(CYN);
		ft_putstr(" $> ");
		ft_putstr(RESET);
	}
}

/*
** loop for all work
*/

void			ft_loop(t_env *e)
{
	char	*tmp;
	char	*linetmp;

	while (42)
	{
		signal(SIGINT, signal_handler);
		if (e->path != NULL)
			ft_freearr(e->path);
		if (e->args != NULL)
			ft_freearr(e->args);
		tmp = ft_getenv("PATH", e);
		ft_printp(e);
		linetmp = ft_getline();
		e->line = ft_strtrim(linetmp);
		e->args = ft_strsplit(e->line, ' ');
		e->path = ft_strsplit(tmp, ':');
		ft_sokrati(linetmp, tmp);
		if (e->args[0] == '\0' || ft_builtins(e) == 1)
				;
		else
			ft_fork(e);
		ft_fin_free(e->line);
	}
}
