/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_signal.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/22 13:05:13 by vbudnik           #+#    #+#             */
/*   Updated: 2018/05/25 19:29:11 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/minishell.h"

/*
** function for hadler signal and print short name of minishell
*/

void	signal_handler(int sig)
{
	if (sig == SIGINT)
	{
		ft_putstr(CYN);
		ft_putstr("\n");
		ft_putstr(" $> ");
		ft_putstr(RESET);
		signal(SIGINT, signal_handler);
	}
}
