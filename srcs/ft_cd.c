/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cd.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/21 19:16:46 by vbudnik           #+#    #+#             */
/*   Updated: 2018/05/22 13:14:41 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/minishell.h"

/*
** get path for change
*/

static char		*get_path(char *str)
{
	int		i;
	int		j;
	char	*path;

	i = 0;
	j = 0;
	if (!(path = (char *)malloc(sizeof(char) * (ft_strlen(str) + 1))))
		return (NULL);
	while (str[i] != '\0')
	{
		if (str[i] == '~')
			i++;
		path[j] = str[i];
		i++;
		j++;
	}
	path[j] = '\0';
	return (path);
}

/*
** my realization of cd commands
*/

void			ft_cd(t_env *e)
{

	// check $ in env
	
	e->cd_path = NULL;
	e->home = NULL;
	e->pwd = NULL;
	e->old_pwd = NULL;
	e->home = ft_getenv("HOME", e);
	e->old_pwd = ft_getenv("OLD", e);
	e->pwd = ft_getenv("PWD", e);
	ft_setenv("OLDPWD", e->pwd, 1, e);
	if (e->args[1] != NULL)
	{
		e->cd_path = get_path(e->args[1]);
		if (e->args[1][0] == '~')
			chdir(ft_strcat(e->home, e->cd_path));
		else if (e->args[1][0] == '-')
			chdir(e->old_pwd);
		else
			chdir(e->cd_path);
	}
	else
		chdir(e->home);
	ft_setenv("PWD", getcwd(e->cwd, sizeof(e->cwd)), 1, e);
	free(e->home);
	free(e->cd_path);
	free(e->old_pwd);
	free(e->pwd);
}
