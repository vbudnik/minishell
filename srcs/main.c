/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/21 18:33:02 by vbudnik           #+#    #+#             */
/*   Updated: 2018/05/22 16:36:28 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/minishell.h"

/*
** function for init struct env and get env from system
*/

void	init(t_env *e)
{
	extern char		**environ;
	int				count;
	int				i;

	i = 0;
	count = ft_count_arr(environ);
	e->line = NULL;
	e->args = NULL;
	e->cd_path = NULL;
	e->home = NULL;
	e->pwd = NULL;
	e->old_pwd = NULL;
	e->exit = "exit";
	if (!(e->env = (char **)malloc(sizeof(char *) * (count + 2))))
		return ;
	while (environ[i] != NULL)
	{
		e->env[i] = (char *)malloc(sizeof(char) * (ft_strlen(environ[i]) + 1));
		ft_strcpy(e->env[i], environ[i]);
		i++;
	}
	e->env[i] = NULL;
}

/*
** set baner for minishell
*/

void	baner(void)
{
	ft_putstr(BLU);
	ft_putendl("              _       _      __         ____");
	ft_putendl("   ____ ___  (_)___  (_)____/ /_  ___  / / /");
	ft_putendl("  / __ `__ \\/ / __ \\/ / ___/ __ \\/ _ \\/ / /");
	ft_putendl(" / / / / / / / / / / (__  ) / / /  __/ / / ");
	ft_putendl("/_/ /_/ /_/_/_/ /_/_/____/_/ /_/\\___/_/_/");
	ft_putstr("\n");
	ft_putstr(RESET);
}

int		main(void)
{
	t_env	e;

	init(&e);
	baner();
	ft_loop(&e);
	return (0);
}
