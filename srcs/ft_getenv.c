/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getenv.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/21 20:53:40 by vbudnik           #+#    #+#             */
/*   Updated: 2018/05/22 13:15:45 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/minishell.h"

/*
** find and check env from system
*/

static char	*ft_envloop(t_env *e, int i, const char *name, char **t)
{
	char	*ft;

	while (e->env[i])
	{
		t = ft_strsplit(e->env[i], '=');
		if (ft_strncmp(t[0], name, ft_strlen(name)) == 0)
		{
			ft = (char *)ft_memalloc(sizeof(char) * (ft_strlen(t[1] + 1)));
			ft_strcpy(ft, t[1]);
			ft_freearr(t);
			return (ft);
		}
		ft_freearr(t);
		i++;
	}
	return (NULL);
}

/*
** get env and return
*/

char		*ft_getenv(const char *name, t_env *e)
{
	int		i;
	char	**temp;
	char	*str;

	i = 0;
	temp = NULL;
	str = ft_envloop(e, i, name, temp);
	if (str != NULL)
		return (str);
	free(temp);
	return (NULL);
}
