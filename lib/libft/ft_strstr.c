/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/11 14:11:09 by vbudnik           #+#    #+#             */
/*   Updated: 2017/11/11 14:15:21 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./includes/libft.h"

char	*ft_strstr(const char *s1, const char *s2)
{
	char	*cp;
	char	*str1;
	char	*str2;

	cp = (char *)s1;
	if (!*s2)
		return ((char *)s1);
	while (*cp)
	{
		str1 = cp;
		str2 = (char *)s2;
		while (*str1 && *str2 && !(*str1 - *str2))
		{
			str1++;
			str2++;
		}
		if (!*str2)
			return (cp);
		cp++;
	}
	return (0);
}
