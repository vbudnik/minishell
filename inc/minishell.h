/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/21 20:08:39 by vbudnik           #+#    #+#             */
/*   Updated: 2018/05/25 19:20:56 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# include <unistd.h>
# include <stdlib.h>
# include "libft.h"
# include <signal.h>

# define BUF_SIZE 2048

/*
**  Color
*/

# define RED   "\x1B[31m"
# define GRN   "\x1B[32m"
# define YEL   "\x1B[33m"
# define BLU   "\x1B[34m"
# define MAG   "\x1B[35m"
# define CYN   "\x1B[36m"
# define RESET "\x1B[0m"

/*
** env and arguments structure
*/

typedef struct		s_env
{
	char			*line;
	char			*exit;
	char			*home;
	char			*cd_path;
	char			*old_pwd;
	char			*pwd;
	char			**args;
	char			**path;
	char			cwd[512];
	char			**env;
	char			**origin_env;
}					t_env;

/*
** for loop read, get, join and free var
*/

char				*ft_getline(void);
void				ft_freeargs(t_env *v);
void				ft_loop(t_env *v);
int					ft_count_arr(char **arr);
char				*ft_argsjoin(char **args);
void				ft_freearr(char **arr);
char				*get_name(void);
void				signal_handler(int sig);

/*
** my function for start
*/

void				ft_cd(t_env *v);
void				ft_env(t_env *v);
char				*ft_getenv(const char *name, t_env *v);
int					ft_setenv(const char *name, const char *value,
			int overwrite, t_env *v);
int					ft_unsetenv(const char *name, t_env *v);
void				ft_exit(t_env *v);
int					ft_echo(t_env *v);
int					ft_fork(t_env *v);

/*
** my function for norm
*/

int					ft_myfunc(char *str);
void				ft_sokrati(char *str, char *tmp);
void				ft_fin_free(char *str);

#endif
