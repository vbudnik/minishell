#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/11/04 18:32:00 by vbudnik           #+#    #+#              #
#    Updated: 2018/05/22 13:18:21 by vbudnik          ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME			:=			minishell

SRC_DIR			:=			./srcs/
OBJ_DIR			:=			./obj/
INC_DIR			:=			./inc/
LIB_DIR			:=			./lib/

SRC 			:=			ft_args.c ft_cd.c ft_echo.c ft_env.c ft_fork.c ft_getline.c ft_loop.c main.c ft_getenv.c ft_signal.c ft_tehfunc.c

OBJ 			=			$(addprefix $(OBJ_DIR), $(SRC:.c=.o))

LIBFT			=			$(LIBFT_DIR)libft.a
LIBFT_DIR		:=			$(LIB_DIR)libft/
LIBFT_INC		:=			$(LIBFT_DIR)includes/
LIBFT_FLAGS		:=			-lft -L $(LIBFT_DIR)

CC_FLAGS		:=			-Wall -Wextra -Werror

LINK_FLAGS		:=			$(LIBFT_FLAGS)

HEADER_FLAGS	:=			-I $(INC_DIR)  -I $(LIBFT_INC)

CC				:=			gcc

all: $(NAME)

$(NAME): $(LIBFT) $(OBJ)
	$(CC) $(OBJ) $(LINK_FLAGS) -o $(NAME)

$(OBJ): | $(OBJ_DIR)

$(OBJ_DIR):
	 mkdir $(OBJ_DIR)

$(OBJ_DIR)%.o: %.c
	$(CC) -c $< -o $@ $(CC_FLAGS) $(HEADER_FLAGS)

$(LIBFT):
	make -C $(LIBFT_DIR)

clean:
	rm -f $(OBJ)
	make clean -C $(LIBFT_DIR)

fclean: clean
	rm -f $(NAME)
	rm -rf $(OBJ_DIR)
	make fclean -C $(LIBFT_DIR)

re: fclean all
vpath %.c $(SRC_DIR)
